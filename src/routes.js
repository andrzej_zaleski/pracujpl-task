export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/list/1');

  $stateProvider
    // thanks to angular-ui-router other routes can be added like '/home'
    .state('home', {
      url: '/home',
      template: '<main style="display: flex; justify-content: center; align-items: center; height: 100vh"><a ui-sref="list.page({page: 1})">See the list</a></main>'
    })
    .state('list', {
      abstract: true,
      url: '',
      resolve: {
        downloadedBuilds: travisService => {
          return travisService.getBuilds();
        }
      },
      controller: ($scope, downloadedBuilds) => {
        $scope.travisBuilds = downloadedBuilds.data;
      },
      template: '<builds-list safe-src="travisBuilds"></builds-list>'
    })
    .state('list.page', {
      url: '/list/:page'
    });
}
