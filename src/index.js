import angular from 'angular';
import 'angular-smart-table';
import 'angular-ui-router';
import 'foundation-sites/dist/foundation.css';

import {BuildsList} from './app/components/BuildsList';
import {BuildsService} from './app/services/builds';
import {TravisService} from './app/services/travis';
import routesConfig from './routes';

import './index.scss';

angular
  .module('app', ['ui.router', 'smart-table'])
  .config(routesConfig)
  .service('travisService', TravisService)
  .service('buildsService', BuildsService)
  .component('buildsList', BuildsList);
