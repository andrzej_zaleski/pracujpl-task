export class TravisService {

  /** @ngInject */
  constructor($http) {
    this.$http = $http;
  }

  getBuilds() {
    return this.$http({
      method: 'GET',
      url: "https://api.travis-ci.org/repos/sinatra/sinatra/builds"
    });
  }
}
