export class BuildsService {

  /** @ngInject */
  constructor($filter, $state) {
    this.$filter = $filter;
    this.$state = $state;
  }

  getFiltredBuildsList(tableState, source) {
    if (tableState && tableState.search && tableState.search.predicateObject) {
      return this.$filter('filter')(source, tableState.search.predicateObject);
    }
    return source;
  }

  getNoDisplayedPages(filteredBuilds, itemsByPage) {
    let displayedPages = parseInt(filteredBuilds.length / itemsByPage, 10);
    displayedPages += filteredBuilds.length % itemsByPage > 0 ? 1 : 0;
    return displayedPages;
  }

  validateRoute(routePageValue, dispalyedPages) {
    // 1. if 'page' param in '/list/:page' is not a number then go to 'list/1'
    if (isNaN(routePageValue)) {
      return false;
    }

    // 2. now thanks to 1. we are sure that 'page' param in '/list/:page' is a number
    // so we can check if this page is not bigger than counted number of dispalyedPages for
    // the whole downoladed set of builds and itemsByPage setup
    // i.e. we have 5 pages for 25 builds and for 5 itemsByPage so when user puts '/list/6', '/list/7', ...
    // then he will be redirected to 'list/1'
    if (dispalyedPages < parseInt(routePageValue, 10)) {
      return false;
    }

    return true;
  }

  getPaginationStart(routePageValue, itemsByPage) {
    // Finally when 'page' param in '/list/:page' is a number and 'page' is not bigger than dispalyedPages thanks to executing validatedRoute
    // we can setup pagination start value
    // i.e. for 'list/1' -> (1-1) * 5 = 0, for 'list/2' -> (2-1) * 5 = 5, .....
    return (parseInt(routePageValue, 10) - 1) * itemsByPage;
  }

  getPageRouteAndPaginationStart(initPaginationStart, itemsByPage, lastPageToGetBack) {
    let pageRouteToGo = parseInt(initPaginationStart / itemsByPage, 10) + 1;

    if (lastPageToGetBack) {
      // update pageRouteToGo to lastPageToGetBack
      pageRouteToGo = lastPageToGetBack;

      return [pageRouteToGo, this.getPaginationStart(pageRouteToGo, itemsByPage)];
    }

    return [pageRouteToGo, initPaginationStart];
  }

}
