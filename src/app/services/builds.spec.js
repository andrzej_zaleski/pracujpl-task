import angular from 'angular';
import 'angular-mocks';

import {BuildsService} from './builds';
import {mockedBuilds} from './mockedBuilds';

describe('TodoService', () => {
  let sourceBuilds;
  let buildsService;

  beforeEach(() => {
    sourceBuilds = mockedBuilds;
  });

  it('1. should return 8 "push" event-type commits fromt the predefined source builds list', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const tableState = {
      search: {
        predicateObject: {
          $: "push"
        }
      }
    };
    const filtered = buildsService.getFiltredBuildsList(tableState, sourceBuilds);
    expect(filtered.length).toEqual(8);
  }));

  it('2. should return whole predefined source builds list if predicateObject empty', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const tableState = {
      search: {
        predicateObject: {
          $: ""
        }
      }
    };
    const filtered = buildsService.getFiltredBuildsList(tableState, sourceBuilds);
    expect(filtered.length).toEqual(25);
  }));

  it('3. should return whole predefined source builds list if no predicateObject at all', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const tableState = {
      search: {}
    };
    const filtered = buildsService.getFiltredBuildsList(tableState, sourceBuilds);
    expect(filtered.length).toEqual(25);
  }));

  it('4. should return 5 diplayed pages for 25 length list with 5 builds per page', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const tableState = {
      search: {}
    };
    const filtered = buildsService.getFiltredBuildsList(tableState, sourceBuilds);

    const displayedPages = buildsService.getNoDisplayedPages(filtered, 5);
    expect(displayedPages).toEqual(5);
  }));

  it('5. should return 7 diplayed pages for 25 length list with 4 builds per page', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const tableState = {
      search: {}
    };
    const filtered = buildsService.getFiltredBuildsList(tableState, sourceBuilds);

    const displayedPages = buildsService.getNoDisplayedPages(filtered, 4);
    expect(displayedPages).toEqual(7);
  }));

  it('6. should return false during validation of "6" page value in route for 5 displayed pages', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const validation = buildsService.validateRoute('6', 5);
    expect(validation).toEqual(false);
  }));

  it('7. should return false during validation of "d34łdd" page value in route', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const validation = buildsService.validateRoute('d34łdd', 5);
    expect(validation).toEqual(false);
  }));

  it('8. should setup start of pagination at 5 index for 2 page active (route page - 2)', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const paginationStart = buildsService.getPaginationStart('2', 5);
    expect(paginationStart).toEqual(5);
  }));

  it('9. should setup start of pagination at 0 index for 1 page active (route page - 1)', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const paginationStart = buildsService.getPaginationStart('1', 5);
    expect(paginationStart).toEqual(0);
  }));

  it('10. should return [pageRouteToGo, tableStart] = [4, 15] for last page 4 displayed (cached value) before search execution regardless of initPaginationStart (i.e. 10)', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const paginationStart = buildsService.getPageRouteAndPaginationStart(10, 5, 4);
    expect(paginationStart).toEqual([4, 15]);
  }));

  it('11. should return [pageRouteToGo, tableStart] = [3, 10] if no previous cached displayed page and initPaginationStart is 10', angular.mock.inject($filter => {
    buildsService = new BuildsService($filter);
    const paginationStart = buildsService.getPageRouteAndPaginationStart(10, 5, undefined);
    expect(paginationStart).toEqual([3, 10]);
  }));
});
