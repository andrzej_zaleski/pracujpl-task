class BuildsListController {

  /** @ngInject */
  constructor($location, $state, buildsService) {
    this.$location = $location;
    this.$state = $state;
    this.BuildsService = buildsService;

    this.itemsByPage = 5;
    this.firstPipeDone = false;

    this.pipe = tableState => this.pipeDefintion(tableState);
  }

  cacheLastPage(tableState) {
    if (Object.keys(tableState.search.predicateObject).length === 1) {
      if (!this.lastPageCache) { // only if not setup already
        this.lastPageCache = this.$state.params.page;
      }
    } else {
      this.lastPageToGetBack = this.lastPageCache;
      delete this.lastPageCache;
    }
  }

  paginationAndRoutesSetup(tableState, dispalyedPages, itemsByPage, routePageValue) {
    if (this.firstPipeDone === false) {
      // called only when pipe function from angular-smart-table is launched for the first time
      // given route param initate tableState.pagination.start value to indicate rigth page on links navigation
      const routeValid = this.BuildsService.validateRoute(routePageValue, dispalyedPages);
      if (routeValid === false) {
        this.$state.go('list.page', {page: 1});
        this.firstPipeDone = true;
      } else {
        // pagination start index setup
        tableState.pagination.start = this.BuildsService.getPaginationStart(routePageValue, itemsByPage);
        this.firstPipeDone = true;
      }
    } else {
      // called always when pipe function launched but not during first time
      // given tableState.pagination.start OR/AND LAST CACHED page when search input was triggered
      // -> setup route param accordingly and optionally tableState.pagination.start
      let pageRouteToGo = 1;
      [pageRouteToGo, tableState.pagination.start] = this.BuildsService.getPageRouteAndPaginationStart(tableState.pagination.start, itemsByPage, this.lastPageToGetBack);

      // empty cache
      delete this.lastPageToGetBack;

      // route page adjustemnt
      this.$state.go('list.page', {page: pageRouteToGo});
    }
  }

  pipeDefintion(tableState) {
    // 1. filter builds based on tableState.search.predicateObject or get all downloaded ones
    const filteredBuilds = this.BuildsService.getFiltredBuildsList(tableState, this.safeSrc);

    // 2. cache last page route before presenting filtered values - in order to get back to it when search input will be emptied again
    if (tableState && tableState.search && tableState.search.predicateObject) {
      this.cacheLastPage(tableState);
    }

    // 3. get number of displayed pages in pagination compomenent based on filtered set in 1. and number of items per page
    this.dispalyedPages = this.BuildsService.getNoDisplayedPages(filteredBuilds, this.itemsByPage);
    tableState.pagination.numberOfPages = this.dispalyedPages;

    // 4. setup of pagination start index for current page or route page adjustemnt
    this.paginationAndRoutesSetup(tableState, this.dispalyedPages, this.itemsByPage, this.$state.params.page);

    // 5. slice builds records (i.e. 5 if this.itemsByPage is 5) that will be displayed in table rows for specific page
    this.builds = filteredBuilds.slice(tableState.pagination.start, tableState.pagination.start + this.itemsByPage);
  }
}

export const BuildsList = {
  templateUrl: 'app/components/BuildsList.html',
  controller: BuildsListController,
  bindings: {
    // loaded from travisAPI during resole of abstract state 'list'
    safeSrc: '<'
  }
};
